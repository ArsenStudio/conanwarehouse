<?php

if (!function_exists('sortable_parameter')) {
    function sortable_parameter($column, $direction)
    {
        return $column.'_'.$direction;
    }
}
