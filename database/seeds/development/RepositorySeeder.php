<?php

namespace Seed\Development;

use App\Enums\VisibilityType;
use App\Models\Repository;
use App\Models\User;
use Illuminate\Database\Seeder;

class RepositorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $admin = User::where('email', 'admin@example.com')->first();

        $publicRepo = Repository::firstOrNew([
            'name' => 'public-repo-test',
            'path' => 'public-repo-test'
        ], [
            'description' => 'admin',
            'visibility' => VisibilityType::PUBLIC,
        ]);
        $publicRepo->owner()->associate($admin);
        $publicRepo->save();

        $privateRepo = Repository::firstOrNew([
            'name' => 'private-repo-test',
            'path' => 'private-repo-test'
        ], [
            'description' => 'admin',
            'visibility' => VisibilityType::PRIVATE,
        ]);
        $privateRepo->owner()->associate($admin);
        $privateRepo->save();
    }
}
