<?php

namespace Seed\Development;

use App\Models\User;
use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class AdminSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = User::firstOrNew([
            'email' => 'admin@example.com'
        ], [
            'name' => 'admin',
            'password' => Hash::make('admin'),
        ]);
        $user->markEmailAsVerified();
        $user->save();
    }
}
