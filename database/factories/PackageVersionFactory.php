<?php

use Faker\Generator as Faker;

$factory->define(\App\Models\PackageVersion::class, function (Faker $faker) {
    return [
        'version_string' => $faker->numerify('#.#.#'),
        'channel' => 'stable'
    ];
});

$factory->afterCreating(\App\Models\PackageVersion::class, function (\App\Models\PackageVersion $version, Faker $faker) {
    \Illuminate\Support\Facades\File::copyDirectory(storage_path("framework/testing/resources/package/export"), storage_path("framework/testing/disks/local/".$version->recipeFolderPath));
});
