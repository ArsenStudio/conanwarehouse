<?php

use Faker\Generator as Faker;

$factory->define(\App\Models\Repository::class, function (Faker $faker) {
    return [
        'name' => str_random(20),
        'description' => $faker->text(255),
        'path' => $faker->word,
        'owner_type' => \App\Models\User::class,
        'owner_id' => function () {
            return factory(\App\Models\User::class)->create()->id;
        },
        'visibility' => \App\Enums\VisibilityType::PUBLIC
    ];
});
