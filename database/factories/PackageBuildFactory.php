<?php

use Faker\Generator as Faker;

$factory->define(\App\Models\PackageBuild::class, function (Faker $faker) {
    return [
        'conan_id' => str_random(32),
    ];
});

$factory->afterCreating(\App\Models\PackageBuild::class, function (\App\Models\PackageBuild $build, Faker $faker) {
    \Illuminate\Support\Facades\File::copyDirectory(storage_path("framework/testing/resources/package/packages/5ab84d6acfe1f23c4fae0ab88f26e3a396351ac9"), storage_path("framework/testing/disks/local/".$build->folderPath));
});