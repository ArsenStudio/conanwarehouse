<?php

namespace Tests\Feature\ConanApi;

use App\Models\Repository;
use Illuminate\Http\Response;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Storage;
use Laravel\Passport\Passport;
use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class BuildUploadTest extends TestCase
{
    use RefreshDatabase;

    public function testGetUploadUrls()
    {
        Storage::fake();

        // GIVEN
        $repository = factory(Repository::class)->create();
        $package = factory(\App\Models\Package::class)->create([
            "repository_id" => $repository->id
        ]);
        $version = factory(\App\Models\PackageVersion::class)->create([
            "package_id" => $package->id
        ]);
        $conanId = str_random(32);

        Passport::actingAs($repository->owner);

        // WHEN
        $response = $this->postJson(
            "/api/{$repository->id}/v1/conans/{$version->reference->dirRepr()}/packages/{$conanId}/upload_urls",
            [
                'conaninfo.txt' => 150,
                'conanmanifest.txt' => 161,
                'licenses/LICENSE' => 1082,
                'include/args.hxx' => 91171,
            ]
        );

        // THEN
        $response
            ->assertOk()
            ->assertJsonStructure([
                'conaninfo.txt',
                'conanmanifest.txt',
                'licenses/LICENSE',
                'include/args.hxx',
            ]);

        $this->assertDatabaseHas('package_builds', [
            'conan_id' => $conanId,
            'package_version_id' => $version->id
        ]);
    }

    public function testUploadFiles()
    {
        Storage::fake();

        // GIVEN
        $repository = factory(Repository::class)->create();
        $package = factory(\App\Models\Package::class)->create([
            "repository_id" => $repository->id
        ]);
        $version = factory(\App\Models\PackageVersion::class)->create([
            "package_id" => $package->id
        ]);
        $build = factory(\App\Models\PackageBuild::class)->create([
            "package_version_id" => $version->id
        ]);

        Passport::actingAs($repository->owner);

        $files = [
            'conaninfo.txt' => 150,
            'conanmanifest.txt' => 161,
            'licenses/LICENSE' => 1082,
            'include/args.hxx' => 91171,
        ];

        $response = $this->postJson(
            "/api/{$repository->id}/v1/conans/{$version->reference->dirRepr()}/packages/{$build->conan_id}".
            "/upload_urls",
            $files
        );
        $body = $response->json();

        foreach ($body as $filename => $url) {
            // WHEN
            $response = $this->call(
                'PUT',
                $url,
                [],
                [],
                [],
                [],
                UploadedFile::fake()->image($filename, $files[$filename])
            );

            // THEN
            $response->assertOk();

            Storage::assertExists($build->folderPath."/".$filename);
        }
    }
}
