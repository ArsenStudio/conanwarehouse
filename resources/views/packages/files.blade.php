@extends('layouts.main')

@section('main-content')
    <div class="container-fluid">
        @include('packages.partials.navbar')
        @include('packages.partials.header')
        <div class="card col-12 p-0 mb-4">
            <h3 class="card-header">About</h3>
            <div class="card-body">
                <file-tree-view></file-tree-view>
            </div>
        </div>
    </div>
@endsection
