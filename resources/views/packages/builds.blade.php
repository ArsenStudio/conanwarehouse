@extends('layouts.main')

@section('main-content')
    <div class="container-fluid">
        @include('packages.partials.navbar')
        @include('packages.partials.header')
        <div class="card col-12 p-0 mb-4">
            <h3 class="card-header">Versions</h3>
            <div class="card-body">
                <form action="">
                    <div class="input-group mb-3">
                        <div class="input-group-prepend">
                            <button class="btn btn-outline-secondary" type="submit"><i class="fa fa-search"></i></button>
                        </div>
                        <input type="text" class="form-control" placeholder="Search" name="q" value="{{ request('q') }}"
                               aria-label="Search input">
                        <div class="input-group-append">
                            <button class="btn btn-outline-secondary" type="button" data-toggle="collapse"
                                    data-target="#advanced-search-form" aria-expanded="false"
                                    aria-controls="advanced-search-form">
                                Advanced
                            </button>
                        </div>
                    </div>
                    <div class="collapse" id="advanced-search-form">
                        <div class="card card-body mb-4">
                            <div class="row">
                                <div class="col">
                                    <select class="form-control">
                                        <option selected disabled>OS</option>
                                        @foreach(\ConanSettings::osList() as $osName)
                                            <option>{{ $osName }}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="col">
                                    <select class="form-control">
                                        <option selected disabled>Arch</option>
                                        @foreach(\ConanSettings::archList() as $archName)
                                            <option>{{ $archName }}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="col">
                                    <select class="form-control">
                                        <option selected disabled>Compiler</option>
                                        @foreach(\ConanSettings::compilerList() as $compilerName)
                                            <option>{{ $compilerName }}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="col">
                                    <div class="col">
                                        <input type="text" class="form-control" placeholder="Compiler version">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
                <table class="table mb-0">
                    <thead>
                        <tr>
                            <th scope="col">Id</th>
                            <th scope="col">Package version</th>
                            <th scope="col">OS</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($builds as $build)
                            <tr>
                                <td class="w-25">
                                    <a href="{{ route('packages.build', [$package, $build->conan_id]) }}">{{ $build->conan_id }}</a>
                                </td>
                                <td class="w-25">
                                    {{ $build->version->reference }}
                                </td>
                                <td>
                                    @if($build->isWindowsBuild)
                                        <i class="fab fa-windows fa-lg text-windows" data-toggle="tooltip" data-placement="top" title="Windows"></i>
                                    @endif
                                    @if($build->isAppleBuild)
                                        <i class="fab fa-apple fa-lg text-apple" data-toggle="tooltip" data-placement="top" title="Apple"></i>
                                    @endif
                                    @if($build->isLinuxBuild)
                                        <i class="fab fa-linux fa-lg text-tux" data-toggle="tooltip" data-placement="top" title="Linux"></i>
                                    @endif
                                    @if($build->isFreeBSDBuild)
                                        <i class="fab fa-freebsd fa-lg text-windows" data-toggle="tooltip" data-placement="top" title="FreeBSD"></i>
                                    @endif
                                    @if($build->isAndroidBuild)
                                        <i class="fab fa-android fa-lg text-windows" data-toggle="tooltip" data-placement="top" title="Android"></i>
                                    @endif
                                    @if($build->isArduinoBuild)
                                        <i class="fabc fa-arduino fa-lg text-arduino" data-toggle="tooltip" data-placement="top" title="Arduino"></i>
                                    @endif
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
                {{ $builds->links() }}
            </div>
        </div>
    </div>
@endsection
