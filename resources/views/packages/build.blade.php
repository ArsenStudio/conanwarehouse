@extends('layouts.main')

@section('main-content')
    <div class="container-fluid">
        @include('packages.partials.navbar')
        @include('packages.partials.header')
        <div class="card col-12 p-0 mb-4">
            <h3 class="card-header">Info</h3>
            <div class="card-body">
                <table class="table mb-0">
                    <tbody>
                        <tr>
                            <th scope="row" class="w-25">Name</th>
                            <td>
                                {{ $build->conan_id }}
                            </td>
                        </tr>
                        <tr>
                            <th scope="row" class="w-25">Created at</th>
                            <td>
                                {{ $build->created_at }}
                            </td>
                        </tr>
                        <tr>
                            <th scope="row" class="w-25">Updated at</th>
                            <td>
                                {{ $build->updated_at }}
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
        <div class="card col-12 p-0 mb-4">
            <h3 class="card-header">Build info</h3>
            <div class="card-body">
                <h4>Settings</h4>
                <table class="table">
                    <tbody>
                        <tr>
                            <th scope="row" class="w-25">OS</th>
                            <td>{{ $build->os }}</td>
                        </tr>
                        <tr>
                            <th scope="row" class="w-25">Architecture</th>
                            <td>{{ $build->arch }}</td>
                        </tr>
                        <tr>
                            <th scope="row" class="w-25">Build Type</th>
                            <td>Jacob</td>
                        </tr>
                        <tr>
                            <th scope="row" class="w-25">Compiler</th>
                            <td>{{ $build->compiler }}</td>
                        </tr>
                        <tr>
                            <th scope="row" class="w-25">Compiler Version</th>
                            <td>{{ $build->compiler_version }}</td>
                        </tr>
                    </tbody>
                </table>
                <h4>Options</h4>
                <table class="table mb-0">
                    <tbody>
                        @foreach($build->options as $key => $value)
                            <tr>
                                <th scope="row" class="w-25">{{ $key }}</th>
                                <td>{{ (string) $value }}</td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
        <div class="card col-12 p-0">
            <h3 class="card-header">Files</h3>
            <div class="card-body">
                <!-- TODO -->
            </div>
        </div>
    </div>
@endsection
