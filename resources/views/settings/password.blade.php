@extends('layouts.settings')

@section('main-content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-4">
                <h4>Password</h4>
                <p>
                    After a successful password update, you will be redirected to the login page where you can log in
                    with your new password.
                </p>
            </div>
            <form class="col-lg-8" method="post" action="{{ route('settings.password.update') }}">
                @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
                @csrf
                @method('patch')
                <div class="form-group">
                    <label for="input-password">Current password</label>
                    <input type="password" class="form-control" id="input-password" name="current_password" required>
                    <span class="form-text text-muted">
                         You must provide your current password in order to change it.
                    </span>
                </div>
                <div class="form-group">
                    <label for="input-new-password">New password</label>
                    <input type="password" class="form-control" id="input-new-password" name="password" required>
                </div>
                <div class="form-group">
                    <label for="input-confirm-password">Password confirmation</label>
                    <input type="password" class="form-control" id="input-confirm-password" name="password_confirmation" required>
                </div>
                <div>
                    <button class="btn btn-success">
                        Change password
                    </button>
                    <a class="btn btn-link" href="{{ route('password.request') }}">
                        {{ __('Forgot Your Password?') }}
                    </a>
                </div>
            </form>
        </div>

    </div>
@endsection
