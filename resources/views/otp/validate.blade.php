@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center auth-panel">
            <div class="col-lg-5 col-md-8">
                <div class="card">
                    <div class="card-header">@lang('Two-Factor Authentication')</div>

                    <div class="card-body">
                        <form method="POST" action="{{ route('2fa.validate') }}">
                            @csrf

                            <div class="form-group">
                                <label for="one_time_password" class="text-md-right">@lang('Two-factor authentication code')</label>
                                <input id="one_time_password" type="text" class="form-control{{ $errors->has('one_time_password') ? ' is-invalid' : '' }}" name="one_time_password" required autofocus>

                                @if ($errors->has('one_time_password'))
                                    <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('one_time_password') }}</strong>
                                </span>
                                @endif
                            </div>

                            <div class="form-group">
                                <button type="submit" class="btn btn-primary">
                                    Verify code
                                </button>
                            </div>
                        </form>
                    </div>
                </div>
                <div class="text-center mt-3">
                    <h4>@lang('Don’t have your phone ?')</h4>
                    <a href="{{ route('2fa.recovery.form') }}">
                        @lang('Enter a two-factor recovery code')
                    </a>
                </div>
            </div>
        </div>
    </div>
@endsection
