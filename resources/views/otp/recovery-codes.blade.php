@extends('layouts.settings')

@section('main-content')
    <div class="container-fluid">
        <div class="alert alert-success" role="alert">
            A simple success alert—check it out!
        </div>
        <p>
            Should you ever lose your phone or access to your one time password secret, each of these recovery codes
            can be used one time each to regain access to your account. Please save them in a safe place, or you will
            lose access to your account.
        </p>
        <ul>
            @foreach($codes as $code)
            <li class="text-monospace">{{ $code }}</li>
            @endforeach
        </ul>
        <div>
            <a href="{{ route('settings.security') }}" class="btn btn-success">OK</a>
            <a class="btn btn-outline-secondary" download="conan-warehouse-recovery-codes.txt" href="{{ $fileData }}">Download</a>

        </div>
    </div>
@endsection
