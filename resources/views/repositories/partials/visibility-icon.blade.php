@if($repo->visibility == \App\Enums\VisibilityType::PRIVATE)
    <i data-toggle="tooltip" data-placement="left" title="Private - Repository access must be granted explicitly to each user." class="fas fa-lock fa-fw"></i>
@endif
@if($repo->visibility == \App\Enums\VisibilityType::PUBLIC)
    <i data-toggle="tooltip" data-placement="left" title="Public - The repository can be accessed without any authentication." class="fas fa-globe fa-fw"></i>
@endif
