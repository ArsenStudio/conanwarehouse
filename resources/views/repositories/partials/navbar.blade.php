<nav class="navbar navbar-dark bg-conan mb-4">
    <ul class="nav nav-tabs">
        <li class="nav-item">
            <a class="nav-link @if(Request::routeIs('repos.show')) active @endif" href="{{ route('repos.show', $repo) }}">General</a>
        </li>
        @can('admin', $repo)
        <li class="nav-item">
            <a class="nav-link @if(Request::routeIs('repos.members.show')) active @endif" href="{{ route('repos.members.show', $repo) }}">Members</a>
        </li>
        <li class="nav-item">
            <a class="nav-link @if(Request::routeIs('repos.edit')) active @endif" href="{{ route('repos.edit', $repo) }}">Settings</a>
        </li>
        @endcan
    </ul>
</nav>
