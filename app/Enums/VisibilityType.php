<?php

namespace App\Enums;

use BenSampo\Enum\Enum;

final class VisibilityType extends Enum
{
    const PUBLIC = 0;
    const PRIVATE = 1;
}
