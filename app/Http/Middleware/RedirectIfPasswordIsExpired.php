<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class RedirectIfPasswordIsExpired
{
    protected $except_routes = [
        'password.expired.*',
        'logout'
    ];

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (Auth::check() and is_null(Auth::user()->password) and !$request->routeIs($this->except_routes)) {
            return redirect()->route('password.expired.form');
        }

        return $next($request);
    }
}
