<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\Api\UserSearchRequest;
use App\Http\Resources\UserResource;
use App\Models\User;
use Illuminate\Http\Request;

class UserController extends Controller
{
    public function search(UserSearchRequest $request)
    {
        $q = '%'.$request->input('q').'%';
        $users = User::where('name', 'like', $q)->paginate(20);
        return UserResource::collection($users);
    }
}
