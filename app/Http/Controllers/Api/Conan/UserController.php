<?php

namespace App\Http\Controllers\Api\Conan;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class UserController extends Controller
{
    public function authenticate(Request $request)
    {
        list($username, $password) = explode(':', base64_decode(substr($request->header('Authorization'), 6)));
        if (!\Auth::guard('web')->attempt(['name' => $username, 'password' => $password])) {
            return response("Wrong user or password", Response::HTTP_UNAUTHORIZED);
        }
        return $request->user('web')->createToken('Conan Access')->accessToken;
    }

    public function checkCredentials(Request $request)
    {
        if (!\Auth::check()) {
            return response("Logged user needed!", Response::HTTP_UNAUTHORIZED);
        }
        return $request->user()->name;
    }
}
