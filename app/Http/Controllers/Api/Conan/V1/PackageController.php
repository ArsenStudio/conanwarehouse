<?php


namespace App\Http\Controllers\Api\Conan\V1;

use App\Conan\PackageVersionReference;
use App\Conan\Paths;
use App\Http\Controllers\Controller;
use App\Http\Requests\ConanApi\PackageDeleteFileRequest;
use App\Models\PackageVersion;
use App\Models\Repository;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\URL;
use Symfony\Component\Filesystem\Filesystem;

class PackageController extends Controller
{
    public function digest(int $repositoryId, string $reference)
    {
        $repository = Repository::findOrFail($repositoryId);

        $this->authorize('read', $repository);

        $packageVersion = $repository->getPackageVersion(PackageVersionReference::loadFromUrl($reference));
        $manifestPath = $packageVersion->recipeFolderPath."/".Paths::CONAN_MANIFEST;
        if (!Storage::exists($manifestPath)) {
            abort(404, "manifest not found");
        }

        return response()->json([
            Paths::CONAN_MANIFEST => URL::signedRoute('api.conan.package.file_download', [
                'repository_id' => $repositoryId,
                'package_version_id' => $packageVersion->id,
                'filepath' => Paths::CONAN_MANIFEST,
            ])
        ]);
    }

    public function snapshot(int $repository, string $reference)
    {
        $repository = Repository::findOrFail($repository);

        $this->authorize('read', $repository);

        $packageVersion = $repository->getPackageVersion(PackageVersionReference::loadFromUrl($reference));

        $ret = [];
        $fileSystem = new Filesystem();
        foreach ($packageVersion->files as $filename) {
            $basename = $fileSystem->makePathRelative(
                str_start($filename, "/"),
                str_start($packageVersion->recipeFolderPath, "/")
            );
            $basename = substr($basename, 0, -1);

            $ret[$basename] = Cache::rememberForever('file_hash.'.$filename, function () use ($filename) {
                return md5(Storage::get($filename));
            });
        }
        return response()->json($ret);
    }

    public function downloadUrls(int $repositoryId, string $reference)
    {
        $repository = Repository::findOrFail($repositoryId);

        $this->authorize('read', $repository);

        $packageVersion = $repository->getPackageVersion(PackageVersionReference::loadFromUrl($reference));

        $ret = [];
        $fileSystem = new Filesystem();
        foreach ($packageVersion->files as $filename) {
            $basename = $fileSystem->makePathRelative(
                str_start($filename, "/"),
                str_start($packageVersion->recipeFolderPath, "/")
            );
            $basename = substr($basename, 0, -1);

            $ret[$basename] = URL::signedRoute('api.conan.package.file_download', [
                'repository_id' => $repositoryId,
                'package_version_id' => $packageVersion->id,
                'filepath' => $basename,
            ]);
        }
        return response()->json((object) $ret);
    }

    public function downloadFile(int $repository, int $packageVersionId, string $filepath)
    {
        $packageVersion = PackageVersion::findOrFail($packageVersionId);
        return Storage::download(implode('/', [$packageVersion->recipeFolderPath, $filepath]));
    }

    public function uploadUrls(Request $request, int $repositoryId, string $reference)
    {
        $repository = Repository::findOrFail($repositoryId);

        $this->authorize('write', $repository);

        $packageVersion = $repository->getOrCreatePackageVersion(PackageVersionReference::loadFromUrl($reference));


        $ret = [];
        foreach ($request->json() as $filename => $size) {
            $ret[$filename] = URL::signedRoute('api.conan.package.file_upload', [
                'repository_id' => $repositoryId,
                'package_version_id' => $packageVersion->id,
                'filepath' => $filename,
            ]);
        }
        return response()->json((object) $ret);
    }

    public function uploadFile(Request $request, int $repository, int $packageVersionId, string $filepath)
    {
        $packageVersion = PackageVersion::findOrFail($packageVersionId);
        Storage::put(implode('/', [$packageVersion->recipeFolderPath, $filepath]), $request->getContent(true));
    }

    public function destroy(int $repositoryId, string $reference)
    {
        $repository = Repository::findOrFail($repositoryId);

        $this->authorize('write', $repository);

        $packageVersion = $repository->getPackageVersion(PackageVersionReference::loadFromUrl($reference));
        $packageVersion->delete();
    }

    public function deleteFiles(PackageDeleteFileRequest $request, int $repositoryId, string $reference)
    {
        $repository = Repository::findOrFail($repositoryId);

        $this->authorize('write', $repository);

        $packageVersion = $repository->getPackageVersion(PackageVersionReference::loadFromUrl($reference));

        $files = $request->input("files");

        foreach ($files as $file) {
            $fullFilename = implode('/', [$packageVersion->recipeFolderPath, $file]);
            if (!Storage::delete($fullFilename)) {
                abort(404, $file." not found");
            }
        }
    }
}
