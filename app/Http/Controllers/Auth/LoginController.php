<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Models\SsoIdentity;
use App\Models\User;
use Illuminate\Auth\Events\Registered;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use InvalidArgumentException;
use Laravel\Socialite\Facades\Socialite;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers {
        sendLoginResponse as protected traitSendLoginResponse;
    }

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout', 'redirectToProvider', 'handleProviderCallback');
    }

    /**
     * Attempt to log the user into the application.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return bool
     */
    protected function attemptLogin(Request $request)
    {
        if ($this->guard()->once($this->credentials($request))) {
            if ($this->guard()->user()->otpEnabled) {
                $request->session()->put('intended_login', [
                    'user' => $this->guard()->user(),
                    'remember' => $request->filled('remember'),
                ]);
            } else {
                $this->guard()->login($this->guard()->user(), $request->filled('remember'));
            }
            return true;
        }
        return false;
    }

    /**
     * Send the response after the user was authenticated.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    protected function sendLoginResponse(Request $request)
    {
        $this->clearLoginAttempts($request);

        if ($this->guard()->user()->otpEnabled) {
            return redirect()->route('2fa.validate.form');
        } else {
            return $this->traitSendLoginResponse($request);
        }
    }

    /*
     * 2FA
     */

    /**
     * Show the application's 2FA form.
     *
     * @return \Illuminate\Http\Response
     */
    public function show2faForm(Request $request)
    {
        $user = $request->session()->get('intended_login.user');

        if ($user == null || !$user->otpEnabled) {
            return back();
        }

        return view('otp.validate');
    }

    /**
     * Handle a 2fa validation request to the application.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Http\Response|\Illuminate\Http\JsonResponse
     *
     * @throws \Illuminate\Validation\ValidationException
     */
    public function validate2fa(Request $request)
    {
        $user = $request->session()->get('intended_login.user');

        if ($user == null || !$user->otpEnabled) {
            return abort(404);
        }

        $request->validate([
            'one_time_password' => 'required|string'
        ]);

        $google2fa = app('pragmarx.google2fa');
        if (!$google2fa->verifyGoogle2FA($user->otpSecret, $request->input('one_time_password'))) {
            return redirect()->route('2fa.validate.form')->withErrors(['pin_code' => 'Invalid pin code']);
        }

        $this->guard()->login($user, $request->session()->get('intended_login.remember', false));
        $request->session()->forget('intended_login');
        return $this->traitSendLoginResponse($request);
    }

    public function showRecoveryForm(Request $request)
    {
        $user = $request->session()->get('intended_login.user');

        if ($user == null || !$user->otpEnabled) {
            return back();
        }

        return view('otp.recovery');
    }

    public function recover(Request $request)
    {
        $user = $request->session()->get('intended_login.user');

        if ($user == null || !$user->otpEnabled) {
            return abort(404);
        }

        $request->validate([
            'recovery_code' => 'required|string'
        ]);

        if (!$user->recoveryCodes()->where('code', $request->input('recovery_code'))->exists()) {
            return redirect()->route('2fa.recovery.form')->withErrors(['recovery_code' => 'Invalid recovery code']);
        }

        $this->guard()->login($user, $request->session()->get('intended_login.remember', false));
        $request->session()->forget('intended_login');
        return $this->traitSendLoginResponse($request);
    }

    /*
     * SSO
     */

    /**
     * Redirect the user to the SSO provider authentication page.
     *
     * @param string $provider
     * @return \Illuminate\Http\Response
     */
    public function redirectToProvider(string $provider)
    {
        if (!in_array($provider, config('auth.single_sign_on.providers'))) {
            abort(Response::HTTP_NOT_FOUND);
        }
        try {
            return Socialite::driver($provider)->redirect();
        } catch (InvalidArgumentException $e) {
            abort(Response::HTTP_NOT_FOUND);
        }
    }

    /**
     * Obtain the user information from SSO provider.
     *
     * @param string $provider
     * @return \Illuminate\Http\Response
     */
    public function handleProviderCallback(string $provider)
    {
        if (!in_array($provider, config('auth.single_sign_on.providers'))) {
            abort(Response::HTTP_NOT_FOUND);
        }
        try {
            $driver = Socialite::driver($provider);
        } catch (InvalidArgumentException $e) {
            abort(Response::HTTP_NOT_FOUND);
        }
        $externUser = $driver->user();
        $externUid = $externUser->getId();

        if ($this->guard()->check()) {
            // We connect provider with current authenticated user
            $this->guard()->user()->identities()->updateOrInsert([
                'provider' => $provider,
                'user_id' => $this->guard()->user()->id // required, bug ???
            ], [
                'extern_uid' => $externUid
            ]);
            return redirect()->route('settings.security');
        } else {
            $identity = SsoIdentity::where('provider', $provider)->where('extern_uid', $externUid)->first();

            if ($identity) { // if identity exists, ...
                $this->guard()->login($identity->user); // ... log in the user
                return redirect()->intended($this->redirectPath());
            } else { // if identity does not exists, register a new user (with null password)
                $user = User::create([
                    'name' => $externUser->getName(),
                    'email' => $externUser->getEmail()
                ]);
                $user->identities()->create([
                    'provider' => $provider,
                    'extern_uid' => $externUid
                ]);
                $user->markEmailAsVerified();

                event(new Registered($user));

                $this->guard()->login($user);

                return redirect()->route('password.expired.form');
            }
        }
    }
}
