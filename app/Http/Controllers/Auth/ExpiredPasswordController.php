<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;

class ExpiredPasswordController extends Controller
{

    /**
     * Display the password reset view.
     *
     * If no token is present, display the link request form.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function showResetForm(Request $request)
    {
        return view('auth.passwords.expired')->with(
            ['email' => $request->email]
        );
    }

    /**
     * Reset the given user's password.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function reset(Request $request)
    {
        $validatedData = $request->validate([
            'password' => 'required|confirmed|min:6'
        ]);

        $user = $request->user();

        $user->password = Hash::make($validatedData['password']);
        $user->save();

        return redirect()->intended("/");
    }
}
