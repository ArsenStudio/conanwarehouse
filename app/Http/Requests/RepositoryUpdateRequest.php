<?php

namespace App\Http\Requests;

use App\Enums\VisibilityType;
use BenSampo\Enum\Rules\EnumValue;
use Illuminate\Foundation\Http\FormRequest;

class RepositoryUpdateRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|string',
            'description' => 'nullable|string',
            'visibility' => ['required', 'string', new EnumValue(VisibilityType::class, false)],
        ];
    }
}
