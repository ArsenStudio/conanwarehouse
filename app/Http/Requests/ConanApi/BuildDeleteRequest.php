<?php

namespace App\Http\Requests\ConanApi;

use Illuminate\Foundation\Http\FormRequest;

class BuildDeleteRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            "package_ids" => 'required|array',
            "package_ids.*" => 'string|distinct',
        ];
    }
}
