<?php

namespace App\Http\Requests;

use App\Enums\PermissionType;
use BenSampo\Enum\Rules\EnumValue;
use Illuminate\Foundation\Http\FormRequest;

class RepositoryAddMemberRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'user_ids' => 'required|array|min:1',
            'user_ids.*' => 'required|integer|exists:users,id',
            'permission_level' => ['required', 'integer', new EnumValue(PermissionType::class, false)],
            'expires_at' => 'nullable|date',
        ];
    }
}
