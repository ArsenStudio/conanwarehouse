<?php

namespace App\Observers;

use App\Models\PackageBuild;

class PackageBuildObserver
{
    /**
     * Handle the package build "created" event.
     *
     * @param  \App\Models\PackageBuild  $packageBuild
     * @return void
     */
    public function created(PackageBuild $packageBuild)
    {
        //
    }

    /**
     * Handle the package build "updated" event.
     *
     * @param  \App\Models\PackageBuild  $packageBuild
     * @return void
     */
    public function updated(PackageBuild $packageBuild)
    {
        //
    }

    /**
     * Handle the package build "deleted" event.
     *
     * @param  \App\Models\PackageBuild  $packageBuild
     * @return void
     */
    public function deleted(PackageBuild $packageBuild)
    {
        \Storage::deleteDirectory($packageBuild->folder_path);
    }

    /**
     * Handle the package build "restored" event.
     *
     * @param  \App\Models\PackageBuild  $packageBuild
     * @return void
     */
    public function restored(PackageBuild $packageBuild)
    {
        //
    }

    /**
     * Handle the package build "force deleted" event.
     *
     * @param  \App\Models\PackageBuild  $packageBuild
     * @return void
     */
    public function forceDeleted(PackageBuild $packageBuild)
    {
        //
    }
}
