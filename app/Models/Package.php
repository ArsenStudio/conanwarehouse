<?php

namespace App\Models;

use App\Models\Traits\HasAvatar;
use Illuminate\Database\Eloquent\Model;

class Package extends Model
{
    use HasAvatar;

    protected $fillable = ['name', 'user', 'description', 'visibility'];

    /*
     * Relationships
     */

    public function versions()
    {
        return $this->hasMany(PackageVersion::class);
    }

    public function repository()
    {
        return $this->belongsTo(Repository::class);
    }

    public function builds()
    {
        return $this->hasManyThrough(PackageBuild::class, PackageVersion::class);
    }

    /*
     * Accessors & mutators
     */

    /*
     * Helpers
     */

    public function defaultAvatarUrl()
    {
        return asset('images/package-avatar.png');
    }
}
