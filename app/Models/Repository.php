<?php

namespace App\Models;

use App\Conan\PackageVersionReference;
use App\Enums\VisibilityType;
use App\Models\Traits\HasAvatar;
use Illuminate\Database\Eloquent\Model;

/**
 * @method static Repository findOrFail(int $repositoryId)
 * @method static bool exists(int $repository)
 */
class Repository extends Model
{
    use HasAvatar;

    protected $fillable = ['name', 'path', 'description'];

    public function owner()
    {
        return $this->morphTo();
    }

    public function packages()
    {
        return $this->hasMany(Package::class);
    }

    public function avatar()
    {
        return $this->belongsTo(Media::class, 'avatar_id');
    }

    public function members()
    {
        return $this->belongsToMany(User::class, 'repository_memberships')
            ->using(RepositoryMembership::class);
    }

    /*
     * Helpers
     */

    public function defaultAvatarUrl()
    {
        return asset('images/repo-avatar.png');
    }

    public function getPackageVersion(PackageVersionReference $reference) : PackageVersion
    {
        $package = $this->packages()->where([
            'name' => $reference->name(),
            'user' => $reference->user()
        ])->firstOrFail();

        $packageVersion = $package->versions()->where([
            'version_string' => $reference->version(),
            'channel' => $reference->channel()
        ])->firstOrFail();

        return $packageVersion;
    }

    public function getOrCreatePackageVersion(PackageVersionReference $reference) : PackageVersion
    {
        $package = $this->packages()->firstOrCreate([
            'name' => $reference->name(),
            'user' => $reference->user()
        ], [
            'visibility' => VisibilityType::PUBLIC
        ]);

        $packageVersion = $package->versions()->firstOrCreate([
            'version_string' => $reference->version(),
            'channel' => $reference->channel()
        ]);

        return $packageVersion;
    }
}
