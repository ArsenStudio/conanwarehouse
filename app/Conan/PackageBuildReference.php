<?php


namespace App\Conan;

class PackageBuildReference
{
    /**
     * @var PackageVersionReference
     */
    protected $packageVersionRef;

    /**
     * @var string
     */
    protected $buildId;

    /**
     * @var string
     */
    protected $revision;

    public static function load($str)
    {
        $str = trim($str);
        $fragments = explode(":", $str);
        PackageVersionReference::load($fragments[0]);
        $versionStr = trim($fragments[0]);
        $buildId = trim($fragments[1]);
        $revision = null;
        if (strpos($buildId, '#') !== false) {
            $fragments = explode("#", $buildId, 2);
            $buildId = $fragments[0];
            $revision = $fragments[1];
        }

        return new PackageBuildReference(PackageVersionReference::load($versionStr), $buildId, $revision);
    }

    public function __construct(PackageVersionReference $packageVersionRef, string $buildId, string $revision = null)
    {
        $this->packageVersionRef = $packageVersionRef;
        $this->buildId = $buildId;
        $this->revision = $revision;
    }

    /**
     * @return PackageVersionReference
     */
    public function packageVersionReference(): PackageVersionReference
    {
        return $this->packageVersionRef;
    }

    /**
     * @return string
     */
    public function buildId(): string
    {
        return $this->buildId;
    }

    /**
     * @return string
     */
    public function revision()
    {
        return $this->revision;
    }

    /**
     * @return string
     */
    public function representation()
    {
        $repr = $this->packageVersionRef.':'.$this->buildId;
        if ($this->revision) {
            $repr .= '#'.$this->revision;
        }
        return $repr;
    }

    public function __toString()
    {
        return $this->representation();
    }
}
